var mongoose = require("mongoose");
var templateSchema = new mongoose.Schema({
  template:{type: String},
  name: { type: String },
  address: { type: String },
  status: { type: String },
  price: { type: String },
  type: { type: String },
  description: { type: String },
  additional_information: { type: String },
 
  amenities: { type : String},
  bedrooms: { type : String },
  size: { type : String },
  map_address: { type : String},
  virtual_tour: { type : String},
  analytics_code:{ type : String },
  seo_metatags: {type: String},

  photos: [{ type : String }],
  video: { type : String },
  documents : [{ type : String }],
  floor_plans : [{type : String} ],
  publish: {type: String},
  deleted: {
    type: Boolean,
    default: false
  },
  created_at : {
      type : Date,
      default: Date.now
  }
});

mongoose.model("Template", templateSchema);
