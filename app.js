require('./model/db');

const express = require('express');
var app = express();
const routes = require('./router');
var bodyparser = require('body-parser');
const config = require('config');

if(!config.get('jwtPrivateKey')){
  console.log('JWT Token Not Set');
  process.exit(1);
}
app.use( bodyparser.json() );  
app.use(bodyparser.urlencoded({ extended: true }));
app.use(function(req, res, next) {
    //allow cross origin requests
    res.setHeader(
      "Access-Control-Allow-Methods",
      "POST, PUT, OPTIONS, DELETE, GET"
    );
    res.header("Access-Control-Allow-Origin", "*");
    res.header(
      "Access-Control-Allow-Headers",
      "Origin, X-Requested-With, Content-Type, Accept"
    );
    res.header("Access-Control-Allow-Credentials", true);
    next();
  });

  app.set('port',3000);
  PORT = app.get('port');
  app.use("/api", routes);
  app.use(express.static(__dirname + '/public/uploads'));
  var server = app.listen(PORT, function() {
    var port = server.address().port;
    console.log("Magic happens on port " + port);
  });


